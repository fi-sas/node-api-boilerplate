#!/bin/bash

function modifiedFile {
  git diff --name-only HEAD | grep "^$1" > /dev/null 2>&1
}

if modifiedFile 'package.json' && ! modifiedFile 'package-lock.json' ; then
  echo "ERROR: package.json modified but no package-lock.json staged to commit."
  exit 1
fi
