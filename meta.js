"use strict";

module.exports = function(values) {
	return {
		questions: [
			{
				type: "input",
				name: "ms_name",
				message: "Give a suffix name for service 'fisas_' (ex.: 'communication' or 'accomodation') to create the magic :)",
				//default: 'ms_name'
			},
			/*{
				type: "input",
				name: "scaffoldServiceNames",
				message: "Create scaffold services (enter service names comma delimited)?",
				//default: []
			}*/

		],

		metalsmith: {
			before(metalsmith) {
				const data = metalsmith.metadata();
				/*
				//TODO: Ask for the 'scaffoldServiceNames' to create the service files (./template/services/<serviceName>.service.js)

				if(data.scaffoldServiceNames){
					const fs = require('fs');
					const path = require('path');
					const { COPYFILE_EXCL } = fs.constants;

					let arrServiceNames = data.scaffoldServiceNames.split(',');

					const servicesPath = path.resolve(__dirname,'./template/services/');

					arrServiceNames.forEach(s=>{
						const serviceToCreate = path.resolve(servicesPath,'./'+s+'.service.js');
						fs.copyFileSync(path.resolve(servicesPath,'./profiles.service.js'), serviceToCreate, COPYFILE_EXCL);
					});
				}
				*/

				/*data.redis = data.cacher == "Redis" || data.transporter == "Redis";
				data.hasDepends = (data.needCacher && data.cacher !== 'Memory') || (data.needTransporter && data.transporter != "TCP");*/
			}
		},

		skipInterpolation: [
			//"public/index.html"
		],

		filters: {
			"services/api.service.js": "ms_name",
			"docker-compose.*": "ms_name",
			".gitlab-ci.yml": "ms_name",
			"docker-stack.*": "ms_name",
			"example.*.env": "ms_name",
			"README.md": "ms_name",
		},

		completeMessage: `
To get started:

	cd {{projectName}}
	npm run start:development OR w/ docker: npm run dc:up

		`
	};
};
